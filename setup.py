#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

print(requirements)

setup(
    name="lincalc",
    version="0.0.1",
    description="Laterality Indices Calculator",
    license="BSD",
    keywords="",
    url="",
    packages=find_packages(),
    include_package_data=True,
    install_requires=requirements,
)
