import itertools
import re
import os

import numpy as np
import pandas as pd

from nilearn import image
from tqdm import trange

from lincalc.masks import Masks


class LInCalc(object):
    """
    Base class of the Laterality Indices Calculator

    Parameters
    ----------
    datafiles_paths: str
        Path to .txt files containing direct data paths
    rois_nifti: str
        Path for ROI file
    rois_labels: str
        Path for ROI labels .txt file
    """

    def __init__(self, datafiles_paths,
                 rois_nifti='../data/ROIs/Horn_MMP_HCP_atlas/MMP_in_MNI_corr_resampled_2mm_91x109x91.nii.gz',
                 rois_labels='../data/ROIs/Horn_MMP_HCP_atlas/HCPMMP1_on_MNI152_ICBM2009a_nlin_left-right.txt'):
        self.rois_nifti = rois_nifti
        self.atlas_img = image.load_img(rois_nifti)
        self.atlas_data = self.atlas_img.get_fdata()
        self.n_rois_per_hemisphere = 180
        self.raw_inputs_df = pd.read_csv(datafiles_paths, header=None)
        self.measures = ['voxels', 'intensities']
        self.measures_explicit = {'voxels': 'Voxel count', 'intensities': 'Intensities'}
        self.desc_stats = ['mean', 'sd']
        self.measure_and_stats = ['%s_%s' % j for j in list(itertools.product(self.measures, self.desc_stats))] + [
            'missing_data', ]
        self.percents = [90, 80, 70, 60, 50, 40]
        self.input_filepaths = self._create_subject_info()
        self._labels = self._get_labels(rois_labels)
        self._lis_and_measures()

    def run(self, output_dir, missing_data=True):
        """
        Calculate the results

        Parameters
        ----------
        output_dir : str
            Path to the output directory.
        missing_data : bool, optional
            Whether missing data should be reported as well. By default it is set true.
        """
        self._calculate_laterality_indices()
        self._to_excel(os.path.join(output_dir, 'Excel/', 'calculated_laterality_indices.xlsx'))
        self._to_csv(os.path.join(output_dir, 'CSV/'))
        if missing_data:
            self._save_missing_data(output_dir)

    def _calculate_laterality_indices(self):
        """ Calculating core method
        """

        # tqdm object for progress bar utility (N ROIs, per hemisphere)
        t = trange(1, len(self._labels[:self.n_rois_per_hemisphere]) + 1, desc='ROI', leave=True)

        # Iterate over left hemisphere indices (ROIs)
        # Of course, operations will be performed for both hemispheres
        for index_left_hemi in t:

            # Update status bar
            status = 'ROI #%s/%s' % (index_left_hemi, self.n_rois_per_hemisphere)
            t.set_description(status)
            t.refresh()

            # 1-180 are left-hemisphere ROIs
            # 200-380 are right-hemisphere ROIs
            roi_name = re.sub('_ROI', '', self._labels[index_left_hemi])
            index_right_hemi = index_left_hemi + 200

            # Get masks: left hemisphere, right hemisphere, reference mask;
            masks = Masks(self.atlas_img, index_left_hemi, index_right_hemi, roi_name)

            # For each file (e.g., subject):
            for input_filename in self.input_filepaths:

                # Load the file to particular masks
                masks.load_data_to_masks(input_filename)

                # Check how many zero-value voxels are there (`missing values`)

                # Note, has to use specific .loc way to get Frame's view, not copy,
                # see: https://stackoverflow.com/a/53954986/8877692
                self.lis_output['missing_data'].loc[input_filename, roi_name] = masks['reference'].get_percent_zero()

                # Get Z-max from reference mask
                z_max = masks['reference'].get_zmax()

                # For measures (voxels, intensities):
                # Prepare dictionary for the measures
                lis = {}
                for measure in self.measures:
                    # Prepare list to append to
                    lis[measure] = []

                    # For each threshold:
                    for thr_percent in self.percents:
                        # - Multiply percent per Z-max (get threshold value)
                        thr_value = z_max * (thr_percent / 100.0)
                        # - For each hemisphere:
                        for hemisphere in ('left', 'right'):
                            # + Count voxels above threshold
                            masks[hemisphere].count_voxels(thr_value)
                            # + Get mean value above the threshold
                            masks[hemisphere].get_value(thr_value)

                        # - Calculate LIs: (m_l - m_r) / (m_l + m_r) * 100:
                        # i.e., masks[hemisphere][measure]
                        m_l = masks['left'][measure]
                        m_r = masks['right'][measure]

                        # Prevent division by 0 (which generates NaNs)
                        if (m_l + m_r) != 0:
                            li = (m_l - m_r) / (m_l + m_r) * 100
                        else:
                            li = 0
                        lis[measure].append(li)

                    # * Average LI for voxel counts for thresholds
                    # * Average LI for voxel intensities for thresholds
                    average = np.mean(lis[measure])
                    self.lis_output['%s_mean' % measure].loc[input_filename, roi_name] = average

                    # * SD of LIs of voxel counts for thresholds
                    # * SD of LIs of voxel intensities for thresholds
                    deviation = np.std(lis[measure])
                    self.lis_output['%s_sd' % measure].loc[input_filename, roi_name] = deviation

    def _to_excel(self, output_filepath):
        """
        Saves calculated indices in form of excel .xlsx file

        Parameters
        ----------
        output_filepath: str
            Path and name of saved file
        """
        # Get directory path and create if non-existent
        output_dirpath = os.path.dirname(output_filepath)
        if not os.path.exists(output_dirpath):
            os.makedirs(output_dirpath)
            print('Created output directory path:\n%s' % output_dirpath)

        # Use pandas' ExcelWriter mechanism
        with pd.ExcelWriter(output_filepath) as writer:
            # For measures (voxels, intensities):
            for measure in self.measures:
                # For stats (mean, sd):
                for stat in self.desc_stats:
                    df_name = '%s_%s' % (measure, stat)
                    df = self.lis_output[df_name]

                    # As Excel spreadsheet
                    df.to_excel(writer, sheet_name=df_name, float_format='%.3f')

        print('')
        print('++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('Excel saved as: %s' % output_filepath)

    def _to_csv(self, output_dir):
        # Get directory path and create if non-existent
        output_dirpath = os.path.dirname(output_dir)
        if not os.path.exists(output_dirpath):
            os.makedirs(output_dirpath)
            print('Created output directory path:\n%s' % output_dirpath)

        # For measures (voxels, intensities):
        for measure in self.measures:
            # For stats (mean, sd):
            for stat in self.desc_stats:
                df_name = '%s_%s' % (measure, stat)
                df = self.lis_output[df_name]
                # CSV states for comma separated value
                csv_output_path = os.path.join(output_dirpath, '%s.csv' % df_name)
                df.to_csv(csv_output_path, float_format='%.3f')

                print('')
                print('=================================================')
                print('Result saved to: %s' % csv_output_path)

    def _save_missing_data(self, output_dir):
        """ Save information about missing data as .xlsx and .csv files
        """

        csv_output_path = os.path.join(output_dir, 'CSV/', 'missing_data.csv')
        excel_output_path = os.path.join(output_dir, 'Excel/', 'missing_data.xlsx')
        df = self.lis_output['missing_data']
        with pd.ExcelWriter(excel_output_path) as writer:
            df.to_excel(writer, sheet_name='missing_data', float_format='%.3f')
        df.to_csv(csv_output_path, float_format='%.3f')

        print('')
        print('+=+=+=+=+=+=+')
        print('Missing data saved as Excel and CSV file')

    def _get_labels(self, rois_labels):
        labels = pd.read_csv(rois_labels, index_col=0, sep='\t', header=None)
        return pd.Series(labels[1], index=labels.index, name='Labels')

    def _lis_and_measures(self):
        self.lis_output = {}
        for measure_stat in self.measure_and_stats:
            self.lis_output[measure_stat] = self.subject_info.copy()

    def _create_subject_info(self):
        subject_info = pd.DataFrame()
        for input_file in self.raw_inputs_df[0]:
            subject_info = subject_info.append(
                {'Input_filepath': input_file}, ignore_index=True)
        self.subject_info = subject_info.set_index('Input_filepath')

        return subject_info['Input_filepath']
