import numpy as np
from nilearn import image


class Mask(dict):
    def __init__(self, mask_type, atlas_img, index, roi_name, index_contralateral=None):
        super().__init__()

        self.mask_type = mask_type

        self.roi_name = roi_name
        self.atlas_img = atlas_img
        self.atlas_data = atlas_img.get_fdata()

        self.index = index
        self.index_contralateral = index_contralateral

        self.mask_data = self._get_mask_data()
        self.mask_img = self._get_mask_img()

    def count_voxels(self, thr_value):
        n_voxels = np.sum(self.zstat_data[self.mask_data] > thr_value)
        super().__setitem__('voxels', n_voxels)

    def get_percent_zero(self):
        # Count voxels with 0.0 value within the mask
        n_zero_voxels = np.sum(self.zstat_data[self.mask_data] == 0)
        # Get ratio of missing values
        percent_zero = n_zero_voxels / np.sum(self.mask_data) * 100
        return percent_zero

    def get_value(self, thr_value):

        # Intensity (values) inside a mask
        intensities_inside_mask = self.zstat_data[self.mask_data]
        # Above some threshold
        above_thr = intensities_inside_mask[intensities_inside_mask > thr_value]

        # Check if there are actually any intensities (values) above the threshold.
        # If not, np.mean() would return NaN
        if len(above_thr) != 0:
            # Average value inside mask, above threshold
            intensities = np.mean(above_thr)
        else:
            intensities = 0

        super().__setitem__('intensities', intensities)

    def set_zstat_data(self, zstat_data):
        self.zstat_data = zstat_data

    def get_zmax(self):
        return self.zstat_data[self.mask_data].max()

    def get_zmax_outlier_factor(self):
        return None

    def _get_mask_data(self):
        mask_data = np.zeros(self.atlas_img.shape)

        # Try to use two masks, else use just one (left xor right)
        try:
            mask_data[self.atlas_data == self.index] = 1
            mask_data[self.atlas_data == self.index_contralateral] = 1
        except NameError:
            mask_data[self.atlas_data == self.index] = 1

        return mask_data.astype('bool')

    def _get_mask_img(self):
        return image.new_img_like(self.atlas_img, self.mask_data, self.atlas_img.affine)
