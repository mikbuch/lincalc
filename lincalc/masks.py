from nilearn import image

from lincalc.mask import Mask


class Masks(dict):
    def __init__(self, atlas_img, index_left_hemi, index_right_hemi, roi_name):
        '''
        ref_img is reference image for data shape, it IS NOT
                `reference` in a sense of the two masks joined
                together
        '''
        super().__init__()

        self.m_l = Mask('left', atlas_img, index_left_hemi, roi_name)
        self.m_r = Mask('right', atlas_img, index_right_hemi, roi_name)
        self.m_ref = Mask('reference', atlas_img, index_left_hemi, roi_name, index_right_hemi)

        super().__setitem__('left', self.m_l)
        super().__setitem__('right', self.m_r)
        super().__setitem__('reference', self.m_ref)

    def load_data_to_masks(self, input_path):

        # First, load whole NIfTI file
        zstat_img = image.load_img(input_path)
        zstat_data = zstat_img.get_fdata()

        # Then get values for particular masks
        self.m_l.set_zstat_data(zstat_data)
        self.m_r.set_zstat_data(zstat_data)
        self.m_ref.set_zstat_data(zstat_data)
