import os
import pandas as pd
import wx

from lincalc import LInCalc


class LInCalcGUI(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, title="LInCalc -- Laterality Indices Calculator")

        self.SetIcon(wx.Icon("../lincalc/resources/logotype/logotype_v1/logotype_v1.ico"))
        self.InitUI()
        self.SetSize((695, 150))

    def InitUI(self):
        panel = wx.Panel(self)
        self.currentDirectory = os.getcwd()
        sizer = wx.GridBagSizer(15, 15)

        #########
        # Input

        # Input label
        sizer.Add(wx.StaticText(panel, label='Input filepaths'), pos=(0, 0), flag=wx.TOP|wx.LEFT, border=15)

        # Input textfield
        self.input_path = wx.TextCtrl(panel, size=(350, -1))
        sizer.Add(self.input_path, pos=(0, 1), flag=wx.TOP, border=15)

        # Input buttin
        input_button = wx.Button(panel, label="Select file")
        sizer.Add(input_button, pos=(0, 2), flag=wx.TOP, border=15)

        ##########
        # Output

        # Output label
        sizer.Add(wx.StaticText(panel, label='Output directory'), pos=(1, 0), flag=wx.LEFT, border=15)

        # Output textfield
        self.output_dir = wx.TextCtrl(panel, size=(350, -1))
        sizer.Add(self.output_dir, pos=(1, 1))

        # Output button
        output_button = wx.Button(panel, label="Select dir")
        sizer.Add(output_button, pos=(1, 2))

        ######
        # Run
        run_button = wx.Button(panel, label="Run")
        sizer.Add(run_button, pos=(2, 3), flag=wx.RIGHT, border=15)

        self.process = wx.StaticText(panel, label='')
        sizer.Add(self.process, pos=(2, 2))

        #######
        # Logo
        start_image = wx.Image("../lincalc/resources/logotype/logotype_v1/logotype_v1.png")
        start_image.Rescale(60, 60)
        image = wx.Bitmap(start_image)
        self.png = wx.StaticBitmap(panel, -1, image, style=wx.BITMAP_TYPE_ANY)
        sizer.Add(self.png, pos=(0, 3), span=(2, 1), flag=wx.TOP, border=15)

        panel.SetSizer(sizer)
        input_button.Bind(wx.EVT_BUTTON, self.OnInput)
        output_button.Bind(wx.EVT_BUTTON, self.OnOutput)
        run_button.Bind(wx.EVT_BUTTON, self.OnRun)

    def _check_empty(self):
        paths = pd.read_csv(self.input_path.GetValue(), header=None)
        paths['exists'] = paths[0].apply(lambda x: os.path.exists(x))
        paths = paths.loc[paths['exists'] == False][0]
        paths = paths.tolist()
        return paths

    # EVENTS
    # ------
    def OnOutput(self, event):
        dlg = wx.DirDialog(self, "Choose a directory:")
        if dlg.ShowModal() == wx.ID_OK:
            self.output_dir.SetValue(dlg.GetPath())
        dlg.Destroy()

    def OnInput(self, event):
        flg = wx.FileDialog(self, "Choose a file:")
        if flg.ShowModal() == wx.ID_OK:
            self.input_path.SetValue(flg.GetPath())
        flg.Destroy()

    def OnRun(self, event):
        nonexistent_paths = self._check_empty()
        if len(nonexistent_paths) == 0:
            self.process.SetLabel('Running...')
            self.process.SetForegroundColour((255, 0, 0))
            self.Update()
            # Create base LInCalc class
            lic = LInCalc(self.input_path.GetValue())
            # Calculate laterality indices and save output as Excel file
            lic.run(self.output_dir.GetValue())
            self.process.SetLabel('Done')
            self.process.SetForegroundColour((0, 144, 0))
        else:
            wx.MessageBox("Cannot load files:\n " + "\n".join(nonexistent_paths), "Error with filepaths",
                          wx.OK | wx.ICON_ERROR)
