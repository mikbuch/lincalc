import wx

from lincalc import LInCalcGUI

# Run the program
if __name__ == "__main__":
    app = wx.App()
    frame = LInCalcGUI()
    frame.Show()
    app.MainLoop()
