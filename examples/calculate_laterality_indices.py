from lincalc import LInCalc

# Define paths
datafiles_paths = '../data/input/input_paths/test.txt'
output_filepath = '../data/output/'

# Create base LInCalc class
lic = LInCalc(datafiles_paths)

# Calculate laterality indices and save output as Excel file
lic.run(output_filepath)
