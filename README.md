# LInCalc

Laterality Indices Calculator (LInCalc)

LInCalc can be used in one of two ways: as a Python module or through graphical user interface (GUI).

The required Python version for this module is `Python3.X`.

The input to the algorithm is expected to be a list of files (in `*.txt` format). The list should contain paths to files which are in the `NIfTI` format, and in `MNI152` standard space (i.e., `91×109×91` voxels).

## Installation

In order to install the `lincalc` module use the following command: 

```bash
python setup.py install
```

## Running the LInCalc

### Using LInCalc via GUI

In order to run the GUI run the following commands:


`(1)` a command to install the LInCalc to your system (if you did that before, you can skip that step, but re-doing this command will not affect the working of the module):

```bash
python setup.py install
```

`(2)` navigate to the directory where the GUI script is stored:

```bash
cd examples
```

`(3)` run the GUI script:

```bash
python run_gui.py
```

### Running the LInCalc as a Python module

The example on how to use the LInCalc Python module in your scripts and notebooks is available at: `examples/calculate_laterality_indices.py`

The commands to install and run the module are:

`(1)` to install:

```bash
python setup.py install
```

`(2)` navigate to the examples directory:

```bash
cd examples
```

`(3)` run the example:

```bash
python calculate_laterality_indices.py
```

## Development

In order to develop the module locally: 

```bash
python setup.py develop
```
